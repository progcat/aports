# Contributor: techknowlogick <techknowlogick@gitea.io>
# Maintainer: techknowlogick <techknowlogick@gitea.io>
pkgname=helm
pkgver=3.12.2
pkgrel=0
pkgdesc="The Kubernetes Package Manager"
url="https://helm.sh/"
arch="all"
license="Apache-2.0"
makedepends="bash go"
options="net"
subpackages="$pkgname-bash-completion $pkgname-fish-completion $pkgname-zsh-completion"
source="$pkgname-$pkgver.tar.gz::https://github.com/helm/helm/archive/v$pkgver.tar.gz"

# secfixes:
#   3.6.0-r0:
#     - CVE-2021-21303
#   3.6.1-r0:
#     - CVE-2021-32690

case "$CARCH" in
	# Disable check on 32bit systems due to upstream test "TestPlatformPrepareCommand" that does not account for these platforms
	# s390x fails in a loop
	s390x|x86|armv7|armhf) options="$options !check" ;;
esac

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

export GOFLAGS="$GOFLAGS -modcacherw"
export CGO_ENABLED=0

build() {
	export CGO_ENABLED=0 # breaks on aarch64
	make GOFLAGS="$GOFLAGS" GIT_TAG="v$pkgver" GIT_COMMIT="" GIT_DIRTY=""

	./bin/helm completion bash > $pkgname.bash
	./bin/helm completion fish > $pkgname.fish
	./bin/helm completion zsh > $pkgname.zsh
}

check() {
	make test-unit GOFLAGS="$GOFLAGS"
}

package() {
	install -Dm755 bin/helm -t "$pkgdir/usr/bin"

	install -Dm644 $pkgname.bash "$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 $pkgname.fish "$pkgdir"/usr/share/fish/vendor_completions.d/$pkgname.fish
	install -Dm644 $pkgname.zsh "$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
9a53ed6d17bcd4162a9ff921c6fa1bfa7b7106faae144ef5f5afbaf3b6438eecd7f61eeef36a27abae8ba78adfb1fff206a7328fbb80b3998938f72d7acafe75  helm-3.12.2.tar.gz
"
