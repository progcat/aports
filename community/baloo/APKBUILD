# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=baloo
pkgver=5.108.0
pkgrel=1
pkgdesc="A framework for searching and managing metadata"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later AND ( LGPL-2.1-only OR LGPL-3.0-only )"
depends_dev="
	kbookmarks-dev
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kfilemetadata-dev
	ki18n-dev
	kidletime-dev
	kio-dev
	kjobwidgets-dev
	kservice-dev
	lmdb-dev
	qt5-qtdeclarative-dev
	solid-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/baloo-$pkgver.tar.xz"
subpackages="$pkgname-dbg $pkgname-dev $pkgname-lang"
options="!check" # Tons of broken tests

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/baloo.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	# We don't ship systemd
	rm -r "$pkgdir"/usr/lib/systemd
}

sha512sums="
948ee3a955f5c5438c9a0c7fc05432ec85b5ff35c1f37094bb6f3024b68606961a826a6e86233f063aeef4ab9d44435c9515906631c0df919ef616ce911f6828  baloo-5.108.0.tar.xz
"
