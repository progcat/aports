# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=py3-dnspython
_pyname=dnspython
pkgver=2.4.0
pkgrel=0
pkgdesc="DNS toolkit for Python3"
url="https://www.dnspython.org/"
arch="all"
license="ISC"
makedepends="py3-setuptools python3-dev cython"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="https://github.com/rthalley/dnspython/archive/v$pkgver/dnspython-$pkgver.tar.gz
	0001-ignore-setuptools-scm.patch
	"
builddir="$srcdir/$_pyname-$pkgver"
options="!check" # network tests are failing on CI

replaces="py-dnspython" # Backwards compatibility
provides="py-dnspython=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	python3 setup.py build --cython-compile
}

package() {
	python3 setup.py install --cython-compile --skip-build --root="$pkgdir"
}

sha512sums="
61becb66d049f9a503a4e60195183cd7b2f6dfb67d28aac85e39d26d30abd1eb97b52f368796438d90a4ff9acb441d1e487d5699484c77ec58e3914885d66aca  dnspython-2.4.0.tar.gz
a23e6c450a7b5c3786d488a1e0701922603ea19e50de9e9372d57aec056b320e2b58b062fa1b610c5651709b8b7b6b9960ad4d1455981973f85ce64fc2ca678f  0001-ignore-setuptools-scm.patch
"
