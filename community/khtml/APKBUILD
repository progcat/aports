# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=khtml
pkgver=5.108.0
pkgrel=1
pkgdesc="The KDE HTML library, ancestor of WebKit"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later AND LGPL-2.1-only"
depends_dev="
	giflib-dev
	karchive-dev
	kcodecs-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kjs-dev
	knotifications-dev
	kparts-dev
	ktextwidgets-dev
	kwallet-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	libjpeg-turbo-dev
	perl-dev
	qt5-qtbase-dev
	sonnet-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	gperf
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/khtml-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/khtml.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a4bececf0f78c08625c7739edae2b29d749ade2cb21c4ee9d81b6dd3677a1a0ba0e53d245d7a355d450a2aad5d021a9c1ed0e326697ea54c32e7d25d84e27cbf  khtml-5.108.0.tar.xz
"
